package br.com.senac.servlet;

import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest requisao, HttpServletResponse resposta) throws ServletException, IOException {

        String usuario = requisao.getParameter("user");
        String navegador = requisao.getHeader("User-Agent");

        //Tipo da resposta
        resposta.setContentType("text/html");
        PrintWriter out = resposta.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<b>Olá, " + usuario + " </b><br/>");
        out.println("Seja bem-vindo!!!<br/>");
        out.println("Você está usando o navegador:" + navegador);
        out.println("</body>");
        out.println("</html>");
    }

    @Override
    protected void doPost(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {
        
           //Pegar os paramentros
        String nome = requisicao.getParameter("usuario");
        String senha = requisicao.getParameter("senha");
        
        
        PrintWriter out = resposta.getWriter();

        out.println("<html>");
        out.println("<body>");

        if ((nome != null && senha != null)
                && //Se não for nulo 
                (!nome.trim().isEmpty() && !senha.trim().isEmpty())) { //Se está preenchidos.

            UsuarioDAO dao = new UsuarioDAO();
            Usuario usuario = dao.getByName(nome);

            if (usuario != null && usuario.getSenha().equals(senha)) {
                
                // Caso nao exista sessao, o container vai criar
                // Caso exista, vai somente devolver o objeto
                
                HttpSession session = requisicao.getSession();
                session.setAttribute("user", usuario);
                session.getAttribute("user");
                
                //out.println("Seja bem vindo " + usuario.getNome());
                resposta.sendRedirect("principal.jsp");
                
                
            } else {
                if(usuario == null){
                    
                out.println("Usuario nao cadastrado");
                
                }else{
                out.println("Erro no login!");
                }
                
            }
            out.println("</body>");
            out.println("</html>");

        } else {
           
            resposta.sendRedirect("login.html");

        }
        
            /*
        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setSenha(senha);

        out.println("<html>");
        out.println("<body>");

        if (usuario.getNome().equals("glauberth") && usuario.getSenha().equals("1234")) {

            out.println("Seja bem vindo " + usuario.getNome());

        } else {

            out.println("Falha na autenticacao");

        }
        out.println("</body>");
        out.println("</html>");
        */
    }

}
