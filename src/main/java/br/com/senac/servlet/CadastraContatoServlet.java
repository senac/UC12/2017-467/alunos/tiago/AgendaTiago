/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.ContatoDAO;
import br.com.senac.agenda.model.Contato;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "CadastraUsuarioServlet", urlPatterns = {"/contato/CadastraUsuarioServlet"})
public class CadastraContatoServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    /*
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("id");
        String erro = null;

        Integer id = null;

        if (codigo != null && !codigo.trim().isEmpty()) {
            id = Integer.parseInt(codigo);

        } else {
            erro = "Não foi possivel encontrar contato";
        }

        ContatoDAO dao = new ContatoDAO();
        Contato contato = dao.get(id);

        request.setAttribute("contato", contato);
        request.setAttribute("erro", erro);

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastroUsuario.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String codigo = request.getParameter("id");
        String nome = request.getParameter("nome");
        String telefone = request.getParameter("telefone");
        String celular = request.getParameter("celular");
        String cep = request.getParameter("cep");
        String endereco = request.getParameter("endereco");
        String numero = request.getParameter("numero");
        String bairro = request.getParameter("bairro");
        String cidade = request.getParameter("cidade");
        String email = request.getParameter("email");
        String estado = request.getParameter("estado");

        Integer id = null;
         
        if (codigo != null && !codigo.trim().isEmpty()) {
                id = Integer.parseInt(codigo);

        }
         
            Contato contato = new Contato();
            
            contato.setNome(nome);
            contato.setTelefone(telefone);
            contato.setCelular(celular);
            contato.setEndereco(endereco);
            contato.setCep(cep);
            contato.setNumero(numero);
            contato.setBairro(bairro);
            contato.setCidade(cidade);
            contato.setEmail(email);
            contato.setEstado(estado);

        try {
            ContatoDAO dao = new ContatoDAO();
            dao.salvar(contato);

            String mensagem = "Salvo com Sucesso!";

            request.setAttribute("contato", contato);
            request.setAttribute("mensagem", mensagem);

        } catch (Exception e) {
            String erro = "falha ao cadastrar";
            request.setAttribute("erro", erro);

        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("cadastroUsuario.jsp");
        dispatcher.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
