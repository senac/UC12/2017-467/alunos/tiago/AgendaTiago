/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.agenda.dao;

import br.com.senac.agenda.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sala302b
 */
public class ContatoDAO extends DAO<Contato> {

    @Override
    public void salvar(Contato contato) {

        Connection connection = null;

        try {
            String query;

            if (contato.getId() == 0) {
                query = "INSERT INTO contatos(nome , telefone , celular , cep , endereco , numero , bairro , cidade , estado, email) values (? , ? , ? , ? , ? , ? , ? , ? , ? , ?) ;";

            } else {
                query = "UPDATE contatos set nome = ? , telefone = ? , celular =  ? , cep = ? , endereco = ? , numero = ? , bairro = ? , cidade = ? , estado = ? , email = ? where id = ?; ";
            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getTelefone());
            ps.setString(3, contato.getCelular());
            ps.setString(4, contato.getCep());
            ps.setString(5, contato.getEndereco());
            ps.setString(6, contato.getNumero());
            ps.setString(7, contato.getBairro());
            ps.setString(8, contato.getCidade());
            ps.setString(9, contato.getEstado());
            ps.setString(10, contato.getEmail());

            if (contato.getId() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setId(rs.getInt(1));
            } else {
                ps.setInt(11, contato.getId());
                ps.executeUpdate();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Erro de cadastro!");

        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao fechar o banco");
            }
        }
    }

    @Override
    public void deletar(Contato contatos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Contato> listar() {

        String query = "select * from contatos;";
        List<Contato> lista = new ArrayList<>();
        Connection connection = null;

        try {

            connection = Conexao.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                Contato contato = new Contato();
                contato.setNome(rs.getString("nome"));
                lista.add(contato);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro ao listar contatos");
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao fechar banco.");

            }
        }
        return lista;
    }

    @Override
    public Contato get(int id) {

        Contato contato = null;
        Connection connection = null;
        
        try {
            String query = "select * from contatos WHERE id = ? ; ";
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.first()) {

                contato = new Contato();
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));

            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Erro ao consultar!");
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao fechar o banco de dados!");
            }
        }

        return contato;

    }

    public List<Contato> getByPesquisa(Integer id, String nome, String estado) {

        List<Contato> lista = new ArrayList<>();
        Connection connection = null;

        try {

            StringBuilder sb = new StringBuilder("select * from contatos where 1=1 ");

            if (id != null) {
                sb.append(" and id = ? ");
            }
            if (nome != null && !nome.trim().isEmpty()) {
                sb.append(" and nome like = ? ");
            }
            if (estado != null && !estado.trim().isEmpty()) {
                sb.append(" and estado = ? ");
            }

            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(sb.toString());

            int index = 0;

            if (id != null) {
                ps.setInt( ++index, id);
            }

            if (nome != null && !nome.trim().isEmpty()) {
                ps.setString(++index, "%" + nome + "%");

            }

            if (estado != null && !estado.trim().isEmpty()) {
                ps.setString(++index, estado);
            }

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                
                Contato contato = new Contato();
                contato.setId(rs.getInt("id"));
                contato.setNome(rs.getString("nome"));
                contato.setTelefone(rs.getString("telefone"));
                contato.setCelular(rs.getString("celular"));
                contato.setCep(rs.getString("cep"));
                contato.setEndereco(rs.getString("endereco"));
                contato.setNumero(rs.getString("numero"));
                contato.setBairro(rs.getString("bairro"));
                contato.setCidade(rs.getString("cidade"));                
                contato.setEstado(rs.getString("estado"));
                contato.setEmail(rs.getString("email"));
                lista.add(contato);                
            }

        } catch (Exception e) {
                e.printStackTrace();
            System.out.println("Erro ao listar contatos!!");
        }finally{
            try {
                connection.close();
            } catch (SQLException e) {
                System.out.println("Erro ao fechar o banco!");
            }
        }

        return lista;

    }

}
