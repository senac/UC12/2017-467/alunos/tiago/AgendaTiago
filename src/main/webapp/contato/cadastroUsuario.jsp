<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp"/>

<% Contato contato = (Contato) request.getAttribute("contato");%>
<% String mensagem = (String) request.getAttribute("mensagem");  %>
<% String erro = (String) request.getAttribute("erro");%>


 <% if(mensagem != null) {%>

    <div class="alert alert-success">    
        <%= mensagem%>
    </div>
    <%}%>
    
    
   <% if(erro != null) {%>

    <div class="alert alert-danger">    
        <%= erro%>
    </div>
    <%}%>


<form action="./CadastraUsuarioServlet" method="post">
    <div class="form-group">
        <label for="codigo">C�digo:</label>
        <p><input name="id" type="text" id="codigo" class="form-control col-2" readonly="" value="<%= contato != null ? contato.getId() : "" %>" />
        </p>
    </div>
    
    <hr/>
    
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="nome">Nome Completo</label>
            <input name= "nome" type="text" class="form-control" id="nome" placeholder="Nome" value="<%= contato != null ? contato.getNome() : "" %>"/>
        </div>  
    </div>

    <div class="form-row">
        <div class="form-group col-2">
            <label for="telefone">Telefone</label>
            <input name="telefone" type="text" class="form-control" id="telefone" placeholder="Telefone" value="<%= contato != null ? contato.getTelefone() : "" %>"/>
        </div>

        <div class="form-group col-md-2">
            <label for="celular">Celular</label>
            <input name="celular" type="text" class="form-control" id="celular" placeholder="Celular" value="<%= contato != null ? contato.getCelular() : "" %>"/>
        </div>
    </div>  

    <div class="form-row">
        <div class="form-group col-6">
            <label for="endereco">Endere�o</label>
            <input name="endereco"  type="text" class="form-control" id="endereco" placeholder="Endere�o" value="<%= contato != null ? contato.getEndereco() : "" %>"/>
        </div>

        <div class="form-group col-4">
            <label for="cep">CEP</label>
            <input name="cep" type="text" class="form-control" id="cep" placeholder="CEP" value="<%= contato != null ? contato.getCep() : "" %>"/>
        </div>

        <div class="form-group col-2">
            <label for="numero">Numero</label>
            <input name="numero" type="text" class="form-control" id="numero" placeholder="Numero" value="<%= contato != null ? contato.getNumero() : "" %>"/>
        </div>
    </div>
    <div class="form-row">

        <div class="form-group col-4">
            <label for="bairro">Bairro</label>
            <input name="bairro"  type="text" class="form-control" id="bairro" placeholder="Bairro" value="<%= contato != null ? contato.getBairro() : "" %>"/>      
        </div>

        <div class="form-group col-4">
            <label for="cidade">Cidade</label>
            <input name="cidade"  type="text" class="form-control" id="cidade" placeholder="Cidade" value="<%= contato != null ? contato.getCidade() : "" %>"/>      
        </div>

        <div class="form-group col-4">
            <label for="estado">Estado</label>
            <select type="text" id="estado" name="estado" class="form-control" >
                <option value="<%= contato != null ? contato.getEstado(): "" %>"  selected></option>
                    <option>ES</option>
                    <option>SP</option>
                    <option>RJ</option>
                    <option>MG</option>
                    <option>BH</option>
            </select>
        </div> 
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="email">Email</label>
            <input name="email" type="text" class="form-control" id="email" placeholder="Email" value="<%= contato != null ? contato.getEmail(): "" %>">
        </div>  
    </div>
    <hr/>

    <button type="submit" class="btn btn-primary">Salvar</button>
        <button type="reset"  class="btn btn-danger">Cancelar</button>
</form>

<jsp:include page="../footer.jsp"/>