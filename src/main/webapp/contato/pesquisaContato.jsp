<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Contato"%>
<jsp:include page="../header.jsp"/>

<% List<Contato> lista = (List) request.getAttribute("lista"); %>
<% String mensagem = (String) request.getAttribute("mensagem"); %>

<% if (mensagem != null) {%>

<div class="alert alert-danger">
    <%=mensagem%>
</div>

<%}%>

<fieldset>

    <center> <b><legend style="color: black">Pesquisa de Contatos</legend> </center></b>

<form action="./PesquisaContatosServlet" method="post">
    <div class="form-row">

        <div class="form-group col-md-2">
            <label for="id">Id</label>
            <input name="id" type="text" class="form-control form-control-md" id="id">
        </div>    

        <div class="form-group col-md-4">
            <label for="nome">Nome</label>
            <input name="nome" type="text" class="form-control form-control-md" id="nome">
        </div>  

        <div class="form-group col-md-2">
            <label for="estado">Estado</label>
            <select name="estado" class="form-control form-control-md" id="estado">                
                <option selected="true"></option>
                <option>ES</option>
                <option>SP</option>
                <option>RJ</option>
                <option>MG</option>
                <option>BH</option>
            </select>        
        </div>
        <div class="form-group col-md-4" style="padding-top: 30px">
            <button type="submit" class="btn btn-outline-primary">Pesquisar</button>        
        </div>
</form>  
</fieldset>

<hr/>


<table class="table table-hover">

    <thead>
        <tr>
            <th>C�digo</th> <th>Nome</th> <th>Telefone</th>
            <th>Celular</th><th>Email</th> <th></th>
        </tr>
    </thead>    

    <% if (lista != null && lista.size() > 0) {
            for (Contato c : lista) {
    %>


    <tr>
        <td><%= c.getId()%></td> <td><%= c.getNome()%></td> <td> <%= c.getTelefone()%> </td> <td><%= c.getCelular()%></td> <td><%= c.getEmail()%></td> 

        <td> <a href="./CadastraUsuarioServlet?id="<%=c.getId()%> > 
                <img src="../resources/imagens/Users-Add-User-icon.png"/></a>
        </td>
        <!--  ESTE LINK VAI ENTRAR CONFIRMAR DO MODAL! -->
        <td> <a  <!-- href="../DeletaContatoServlet?id=" <%=c.getId()%> --> 
                <img src="../resources/imagens/Button-Delete-icon.png"/></a>
        </td>


    </tr>


    <% }
    } else {%>

    <tr >
        <td  colspan="2">N�o existem registros!!!</td>
    </tr>

    <%}%>

</table>

<center><button type="reset" class="btn btn-danger" >Cancelar</button</center>   

<jsp:include page="../footer.jsp"/>
